import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styleUrls: ['./hijo.component.css']
})


export class HijoComponent implements OnInit {
@Input() libroHijo!:any;
@Input () animalHijo!:any;

  constructor() { }

  ngOnInit(): void {
  }

}
